<?php
/* @package WordPress
 * @subpackage iamschulz2
 * @since iamschulz2 2.0
 */

get_header(); ?>

<?php
$lO = [];
$pre = [];
$loadRange = 3;
$page = get_query_var('page');
$beforeRange = ($page > 1 ? $page * $loadRange - $loadRange : 0);

$i = 0;
if (have_posts()) : while (have_posts()) : the_post(); //the loop
    $i++;

    if ($i >= $beforeRange) {
        $ID = get_the_ID();

        if ($i < $loadRange + $beforeRange) {
            array_push($pre, $ID);
        } else {
            array_push($lO, $ID);
        }
    }
endwhile; endif;
?>
    <script>
        window.lO = [];
        window.collapse = true;
        window.offset = 0;

        lO.push(
            <?php
            foreach ($lO as $key => $item) {
                echo $item;
                if ($key++ < count($lO) - 1) {
                    echo ',';
                }
            }
            ?>
        );
    </script>

    <div class="main-content">
        <?php
        foreach ($pre as $key => $item) {
            $box = new box(['post_id' => $item, 'collapse' => true]);
        }
        ?>
    </div>

    <noscript>
        <nav class="pagination" aria-label="Pagination Navigation">
            <ul>
                <?php
                if ($page > 1) {
                    $prevHref = '?page=' . ($page - 1);
                    echo '<li><a href="' . $prevHref . '" class="pagination__link is--prev" aria-label="vorherige Seite">vorherige Seite</a></li>';
                }

                if (count($lO) > 0) {
                    $page = ($page ? $page : 1);
                    $nextHref = '?page=' . ($page + 1);
                    echo '<li><a href="' . $nextHref . '" class="pagination__link is--next" aria-label="nächste Seite">nächste Seite</a></li>';
                }
                ?>
            </ul>
        </nav>
    </noscript>

<?php get_footer(); ?>
