<?php
add_theme_support( 'post-thumbnails' );
add_image_size( 'lightbox', 1920, 1080, false);
add_image_size( 'boxed_desktop', 780, 1200, false);
add_image_size( 'boxed_mobile', 690, 690, false);

/**
 * Registering two new thumbnail sizes in wp, to be used in .box srcsets
 *
 * @param $sizes
 * @return array
 */
function boxed_image_size( $sizes ) {
    return array_merge( $sizes, array(
        'lightbox' => __( 'lightbox' ),
        'boxed_desktop' => __( 'boxed - desktop' ),
        'boxed_mobile' => __( 'boxed - mobile' ),
    ) );
}
add_filter( 'image_size_names_choose', 'boxed_image_size' );

/**
 * adjusting image links in posts for https
 *
 * @param string $url - input URL
 * @param {int} $id - post ID
 * @return string - output url
 */
function ssl_post_thumbnail_urls($url, $post_id) {
    //Skip file attachments
    if(!wp_attachment_is_image($post_id)) {
        return $url;
    }

    //Correct protocol for https connections
    list($protocol, $uri) = explode('://', $url, 2);

    if(is_ssl()) {
        if('http' === $protocol) {
            $protocol = 'https';
        }
    } else {
        if('https' === $protocol) {
            $protocol = 'http';
        }
    }

    return $protocol.'://'.$uri;
}
add_filter('wp_get_attachment_url', 'ssl_post_thumbnail_urls', 10, 2);

/**
 * Writes opengraph meta tags for facebook
 */
function social_meta_tags() {
    $post = get_post();
    if (is_single() == true) {
        preg_match_all('/<img [^>]*src=["|\']([^"|\']+)/i', $post->post_content, $images);
        if(!empty($images[1][0])) {
            $og_image = $images[1][0];
        }

        preg_match_all('/<iframe [^>]*src=["|\']([^"|\']+)/i', $post->post_content, $ytlink);
        $youtube = explode('embed/', $ytlink[1][0]);
        if(!empty($youtube[1])) {
            $og_image = 'https://i.ytimg.com/vi/'.$youtube[1].'/maxresdefault.jpg';
            echo '<meta property="og:video" content="https://www.youtube.com/v/'.$youtube[1].'" />';
        }

        echo '<meta property="og:image" content="' . $og_image . '" />';

        $og_description = $post->post_content;
        if (get_extended($post->post_content)['extended']) {
            $og_description = strip_tags(get_extended($post->post_content)['main']);
        }
        echo '<meta property="og:description" content="'.$og_description.'" />';
        echo '<meta property="og:url" content="'.get_the_guid().'" />';
    } else {
        echo '<meta property="og:description" content="'.get_bloginfo('description').'" />';
        echo '<meta property="og:image" content="'.get_bloginfo('template_directory').'/assets/face.png" />';
        echo '<meta property="og:url" content="'.get_site_url().'" />';
    }
}

/**
 * replaces image links with srcsets
 *
 * @param $str
 * @return string
 */
function replace_img($str, $crop)
{
    if (preg_match("/img/i", $str)) {
        if (preg_match_all('/src[ ]?=[ ]?"([^"]+)"/i', $str, $img_matches)) {
            preg_match_all('/class="([^"]+)/i', $str, $cl_matches);
            preg_match_all('/alt="([^"]+)/i', $str, $alt_matches);

            foreach ($img_matches[1] as $i => $match) {
                preg_match('/(wp\-image-)([0-9]*)/', $cl_matches[1][$i], $imgId);
                $mobile_jpg_url = wp_get_attachment_image_src(
                    $attachment_id = $imgId[2],
                    $size = 'boxed_mobile'
                )[0];

                $mobile_source = '';
                if ($mobile_jpg_url) {
                    $mobile_source = '<source type="image/webp" media="(max-width: 767px)" srcset="' . $mobile_jpg_url . '.webp" />'
                        . '<source media="(max-width: 767px)" srcset="' . $mobile_jpg_url . '" />';
                }

                $desktop_jpg_url = wp_get_attachment_image_src(
                    $attachment_id = $imgId[2],
                    $size = 'boxed_desktop'
                )[0];

                $desktop_source = '';
                if ($desktop_jpg_url) {
                    $desktop_source = '<source type="image/webp" media="(min-width: 768px)" srcset="' . $desktop_jpg_url . '.webp" />'
                        . '<source media="(min-width: 768px)" srcset="' . $desktop_jpg_url . '" />';
                }

                $picCl = 'box__img';
                $imgCl = 'box__thumbnail';
                $cropCl = '';

                if ($crop) {
                    $cropCl = ' is--cropped';
                }

                $replacement =  '<picture class="' . $picCl . $cropCl . ' ' . $cl_matches[1][0] . '">'
                                    . $mobile_source
                                    . $desktop_source
                                    . '<img class="' . $imgCl . '" src="' . trim($match) . '" alt="' . $alt_matches[1][$i] . '" />'
                                . '</picture>';

                $str = preg_replace("~(<img\ class(.*){$img_matches[0][$i]}[^>]+>)~i", $replacement, $str);
            }
        }
    }
    return $str;
}

/**
 * replaces youtube embeds with lazy loadable dummies
 *
 * @param $str
 * @return string
 */
function replace_youtube($str)
{
    if (preg_match_all('/<iframe[^>]*src=\"[^\"]*youtu[.]?be.*\/((v|embed)\/)?([a-zA-Z0-9_-]+).*<\/iframe>/i', $str, $yt_matches)) {
        foreach ($yt_matches[3] as $i => $match) {

            $replacement = '<p>'
                    . '<a href="https://youtube.com/watch?v=' . $match . '" class="youtube-player" id="' . $match . '">'
                        . '<picture class="youtube-player__picture">'
                            //. '<source type="image/webp" media="(max-width: 767px)" srcset="//i.ytimg.com/vi_webp/' . $match . '/mqdefault.webp" />'
                            . '<source media="(max-width: 767px)" srcset="//i.ytimg.com/vi/' . $match . '/mqdefault.jpg" />'

                            //. '<source type="image/webp" media="(min-width: 768px)" srcset="//i.ytimg.com/vi_webp/' . $match . '/hqdefault.webp" />'
                            . '<source media="(min-width: 768px)" srcset="//i.ytimg.com/vi/' . $match . '/hqdefault.jpg" />'

                            . '<img class="youtube-player__thumbnail" src="https://i.ytimg.com/vi/' . $match . '/hqdefault.jpg" alt="YouTube-Video (ID: ' . $match . ')" />'
                        . '</picture>'
                        . '<span class="youtube-player__icon"> </span>'
                    . '</a>'
                . '</p>';

            $str = preg_replace('~(<iframe[^>]*src=\"[^\"]*youtu[.]?be.*\/((v|embed)\/)?(' . $match . ').*<\/iframe>)~i', $replacement, $str);
        }
    }
    return $str;
}

/**
 * creates a .box from a post ID and appends it to the DOM
 *
 * @param {int} $post_id
 * @param {int} $collapse - if post is displayed in short form
 */
class box {
    protected $post_id;
    protected $collapse;
    protected $fadein;
    private $dom;
    private $box;
    private $article;
    private $level;
    private $output;

    function __construct(array $options = array()) {
        $this->post_id = $options['post_id'];
        $this->collapse = $options['collapse'];
        $this->fadein = $options['fadein'];

        $this->dom = new DOMDocument('1.0', 'utf-8');
        $this->article = json_decode(write_article($this->post_id, $this->collapse));

        $this->box = $this->boxify();

        $this->dom->appendChild($this->box);
        $this->randomize();

        $this->output = $this->dom->saveHTML();

        echo sprintf($this->output, $this->article->post_content);
    }

    /**
     * Creates an empty article box
     * Needs to be filled with %s
     *
     * @return DOMElement
     */
    function boxify() {
        $box = $this->dom->createElement('article');

        $this->level = rand(1, 10);
        $boxDataLevel = $this->dom->createAttribute('data-level');
        $boxDataLevel->value = $this->level;
        $box->appendChild($boxDataLevel);

        $boxCl = $this->dom->createAttribute('class');
        $boxClNames = ['box'];
        if ($this->fadein) {
            array_push($boxClNames, 'is--fadein');

        }

        foreach ($this->article->post_cats as $cat) {
            array_push($boxClNames, $cat);
        }
        foreach ($boxClNames as $cl) {
            $boxCl->value .= $cl.' ';
        }
        $box->appendChild($boxCl);

        $header = $this->dom->createElement('header');
        $headerCl = $this->dom->createAttribute('class');
        $headerCl->value = "box__header";
        $header->appendChild($headerCl);

        $headline = $this->dom->createElement('h1', $this->article->post_title);
        $headlineCl = $this->dom->createAttribute('class');
        $headlineCl->value = "box__header__title";
        $headline->appendChild($headlineCl);
        $header->appendChild($headline);
        $box->appendChild($header);

        $boxCon = $this->dom->createDocumentFragment();
        $boxCon->appendXML('<div class="box__content">%s</div>');

        $box->appendChild($boxCon);

        $clear = $this->dom->createElement('div', '&nbsp;');
        $clearCl = $this->dom->createAttribute('class');
        $clearCl->value = "clear";
        $clear->appendChild($clearCl);
        $box->appendChild($clear);

        return $box;
    }

    /**
     * Randomizes box design
     */
    function randomize() {
        $styles = [
            ['--level', $this->level],
        ];

        $styleStr = '';
        foreach ($styles as $style) {
            $styleStr .= $style[0].":".$style[1]."; ";
        }

        $styleAtt = $this->dom->createAttribute('style');
        $styleAtt->value = $styleStr;

        $this->box->appendChild($styleAtt);
    }
}


/**
 * returns a json with all important post data
 *
 * @param {int} $id - post ID
 * @param {boolean} $collapse - show excerpt only
 * @return string
 */
function write_article($id, $collapse) {
    if(empty($id)) {
        return "no id";
    }

    $post = get_post($id);

    $post->post_content = do_shortcode($post->post_content);
    $post->post_content = filter_ptags_on_images($post->post_content);
    if($collapse == true) {
        if (get_extended($post->post_content)['extended']) {
            $moreLink = '<a class="box__more" href="'.get_permalink($post->ID).'">Mehr</a>';
            $post->post_content = get_extended($post->post_content)['main'].$moreLink;
        }
    }
    $post->post_content = replace_img($post->post_content, $collapse);
    $post->post_content = replace_youtube($post->post_content);

    $post->post_content = escape_code($post->post_content);
    $post->post_content= wpautop($post->post_content, false);

    $post->post_permalink = get_permalink($id);
    $cats = [];
    foreach (get_the_category($id) as $c) {
        $cat = get_category($c);
        array_push($cats, 'is--'.$cat->name);
    }
    $post->post_cats = $cats;
    $post = json_encode($post);
    return $post;
}


/**
 * Escapes <pre> snippets in posts,
 * wraps each line into a <span>
 *
 * @param $content
 * @return null|string|string[]
 */
function escape_code($content) {
    $content = preg_replace_callback('/<code.*?>(.*?[<code.*?>.*<\/code>]*)<\/code>/imsu',
        function ($matches) {
            $code = '';
            foreach(preg_split("/((\r?\n)|(\r\n?))/", $matches[1]) as $line) {
                $code .= '<span class="code__line">' . htmlentities($line) . '</span>';
            }

            return str_replace($matches[1], $code, $matches[0]);
        },
        $content);

    return $content;
}

/**
 * gets post data from WP
 */
function ajax_load_post() {
    if(empty($_POST['post_id'])) {die("no id");}
    $post_id = sanitize_text_field($_POST['post_id']);
    $collapse = sanitize_text_field($_POST['collapse']);
    new box(['post_id'=>$post_id, 'collapse'=>$collapse, 'fadein'=>true]);
    die();
}
add_action( 'wp_ajax_ajax_load_post', 'ajax_load_post' );
add_action( 'wp_ajax_nopriv_ajax_load_post', 'ajax_load_post' );

function nav_link_swap( $atts, $item, $args ) {
    if (in_array('current-menu-item', $item->classes)) {
        $atts['href'] = '#main';
    }
    return $atts;
}
add_filter( 'nav_menu_link_attributes', 'nav_link_swap', 10, 3 );
