import {lightbox} from "./_lightbox";
import {activateYtPlayer} from "./_ytReplacer";
import {inScreen} from "./_scrolling";

/**
 * Enables all kinds of functions within each box
 * Needs to be called dynamically because of asynchronously loaded boxes.
 *
 * @param el - box element
 */
export const enableBoxEvents = (el) => {
    //focus management
    document.addEventListener('scroll', () => {
        if (inScreen(el)) {
            el.classList.remove("no--focus");
        } else {
            el.classList.add("no--focus");
        }

        let scrollDistance = (window.scrollY + window.innerHeight / 2).toString();
        document.querySelector('#main').style.setProperty('--scrollDistance', scrollDistance);
    });
    el.addEventListener('mouseenter', () => {
        focus(el.dataset.level);
        el.classList.add('is--hovered');
    });
    el.addEventListener('mouseleave', () => {
        el.classList.remove('is--hovered');
    });

    //detect device orientation
    if (window.DeviceOrientationEvent) {
        window.addEventListener("deviceorientation", function () {
            onTilt([event.beta, event.gamma]);
        }, true);
    } else if (window.DeviceMotionEvent) {
        window.addEventListener('devicemotion', function () {
            onTilt([event.acceleration.x * 2, event.acceleration.y * 2]);
        }, true);
    }

    //activate YouTube player elements
    const boxYt = el.querySelectorAll('.youtube-player');
    for (let i = 0; i < boxYt.length; i++) {
        boxYt[i].addEventListener('click', (e) => {
            e.preventDefault();
            activateYtPlayer(boxYt[i], boxYt[i].id);
        });
    }

    //activate Lightbox
    const boxLinks = el.querySelectorAll('.box a'),
        imgTypes = ['jpg' , 'jpeg', 'png', 'bmp', 'gif', 'svg', 'webp'];

    for (let i = 0; i < boxLinks.length; i++) {
        const file    = boxLinks[i].href,
            fileExt = file.split('.'),
            ext     = fileExt[fileExt.length - 1].toLowerCase();

        if (imgTypes.indexOf(ext) > -1) {
            boxLinks[i].dataset.lightbox = true;
            boxLinks[i].addEventListener('click', (e) =>
            {
                e.preventDefault();
                if (document.querySelectorAll('.lightbox').length < 1) {
                    new lightbox(file);
                }
            });
        }
    }

    const onTilt = (tilt) => {
        document.querySelector('.main-content').style.setProperty('--tiltY', tilt[1]);
    }
};

/**
 * Makes all .boxes react to focus changes.
 *
 * @param {int} level
 */
export const focus = (level) => {
    const boxes = document.querySelectorAll('.box');
    for (let i = 0; i < boxes.length; i++) {
        const thislevel = boxes[i].dataset.level,
            fac = Math.min((1.3 - Math.abs((level-thislevel)/10)), 1);

        boxes[i].style.setProperty('--levelOpacity', fac);
    }
    window.focusLevel = level;
};
