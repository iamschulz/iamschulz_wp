require('../sass/additional/style.sass');
require('../sass/critical/critical.sass');

export const loadCSS = (href) => {
    let ss = window.document.createElement('link'),
        ref = window.document.getElementsByTagName('head')[0];

    ss.rel = 'stylesheet';
    ss.href = href;

    //temporarily, set media to something non-matching to ensure it'll fetch without blocking render
    ss.media = 'foo bar';

    ref.parentNode.insertBefore(ss, ref);

    setTimeout(() => {
        //set media back to `all` so that the stylesheet applies once it loads
        ss.media = 'all';
    },0);
};
