import {loadNextPost} from "./_postLoader";

/**
 * Checks if an object is visible on screen.
 *
 * @param {Object} obj - object to be checked
 * @returns {boolean}
 */
export const inScreen = (obj) => {
    const treshold = 50;
    return (
        (window.scrollY + screen.availHeight > obj.offsetTop - treshold) &&
        (window.scrollY < obj.offsetTop + obj.clientHeight + treshold)
    );

};

/**
 * This is the End - my only friend, the end.
 * Changes moreBtn to toTopBtn
 */
export const theEnd = () => {
    const moreBtn = document.getElementById('ctrl-btn');
    moreBtn.innerText = 'nach oben';
    moreBtn.classList.remove('is--more');
    moreBtn.classList.add('is--up');
    window.theend = true;
};

/**
 * Animated scroll to top
 */
export const scrollUp = () => {
    clearInterval(document.scrollingUp);
    let scrollTop = window.pageYOffset;
    if (scrollTop > 0) {
        window.scrollTo(0, scrollTop - (scrollTop / 25));
        document.scrollingUp = window.setInterval(() => scrollUp(), 0);
    }
};

/**
 * This defines moreBtns behaviour.
 */
export const moreBtn = document.getElementById('ctrl-btn');
moreBtn.innerText = 'mehr anzeigen';
moreBtn.classList.remove('is--up');
moreBtn.classList.add('is--more');

moreBtn.addEventListener('click',(ev) => {
    ev.preventDefault();
    if (moreBtn.classList.contains('is--more')) {
        loadNext(5);
    }

    if (moreBtn.classList.contains('is--up')) {
        scrollUp();
    }
});

/**
 * Load specified amount of next posts
 * @param minAmount - minimum loaded posts. if 0, fill screen
 */
export const loadNext = (minAmount = 1) => {
    //if screen is not filled and not theEnd
    if (minAmount === 0 && window.innerHeight > document.body.clientHeight && !window.theend) {
        loadNext(1);
    }

    for (let i = 0; i < minAmount; i++) {
        let next = loadNextPost();
        if (next === false) {
            break;
        }
    }
};

export const loadMore = () => {
    if (window.scrollY + window.innerHeight > document.body.scrollHeight - 200 && !window.theend) {
        loadNextPost();
    }
};

document.addEventListener('scroll',() => {loadMore()});
