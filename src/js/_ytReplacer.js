/**
 * Switches youtube player replacement for actual iframe player
 *
 * @param el {object} - youtube replacement node
 * @param ytId {string} - youtube ID
 */
export const activateYtPlayer = (el, ytId) => {
    let playerOpts = {
        videoId: ytId,
        playerVars: {
            'autoplay': 1,
            'modestbranding': 1,
            'rel': 0,
            'color': 'white'
        },
        events: {
            'onReady': onPlayerReady,
        }
    };

    function onPlayerReady(event) {
        event.target.playVideo();
    }

    el.classList.add('is--loaded');

    if (window.onYouTubePlayerAPIReady) {
        /*eslint-disable*/
        new YT.Player(ytId, playerOpts);
        /*eslint-enable*/
    } else {
        const tag = document.createElement('script');
        tag.src = "https://www.youtube.com/player_api";
        const firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        window.onYouTubePlayerAPIReady = () => {
            /*eslint-disable*/
            new YT.Player(ytId, playerOpts);
            /*eslint-enable*/
        }
    }
};
