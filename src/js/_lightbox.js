export class lightbox {
    /**
     * Lightbox class. Gets called from links to images.
     * Invoking this class constructs a lightbox element with a given image file and binding the next click to its destructor.
     *
     * @param {string} file - URL to image file.
     */
    constructor(file) {
        this.el = document.createElement('div');
        this.el.classList.add('lightbox');
        this.file = file;

        this.image = this.createImageElement();

        this.initLoadingIndicator();

        document.body.appendChild(this.el);
        document.body.classList.add('is--no-scroll');

        window.setTimeout(() => {
            this.el.classList.add('is--shown');
        }, 10);

        this.el.addEventListener('click', () => {
            this.destruct();
        });

        this.bindKeyHandler = (e) => this.keyHandler(e);
        window.addEventListener('keyup', this.bindKeyHandler);
    }

    /**
     * Adds picture Srcset to Lightbox
     *
     * @returns {HTMLPictureElement}
     */
    createImageElement() {
        const pictureEl = document.createElement('picture');

        const webpSrc = document.createElement('source');
        webpSrc.setAttribute('srcset', this.file + '.webp');
        webpSrc.setAttribute('type', 'image/webp');
        pictureEl.appendChild(webpSrc);

        const image = document.createElement('img');
        image.classList.add('lightbox__image');
        image.setAttribute('src', this.file);
        pictureEl.appendChild(image);

        this.el.appendChild(pictureEl);

        return pictureEl;
    }

    /**
     * Sets Loading class to Lightbox El
     */
    initLoadingIndicator() {
        const image = this.image.querySelector('img');
        if (image.complete) {
            this.el.classList.remove('is--loading');
        } else {
            this.el.classList.add('is--loading');
            image.onload = () => {
                this.el.classList.remove('is--loading');
            };
        }
    }

    /**
     * Handles keyboard navigation for the lightbox
     *
     * @param {object} e - browser event
     */
    keyHandler(e) {
        //on Esc
        if (e.keyCode === 27) {
            this.destruct();
        }

        //on Left, Up
        if (e.keyCode === 37 || e.keyCode === 38) {
            this.goToImage(-1);
        }

        //on Right, Down
        if (e.keyCode === 39 || e.keyCode === 40) {
            this.goToImage(1);
        }
    }

    /**
     * Deletes current pictures and loads next or previous one into lightbox
     * Only counts in the lightbox-enabled images of the current article.
     *
     * @param position {int} - 0 is the current image, negative values previous images, positive values next ones.
     * @returns {boolean} - false return to exit the function if no image is found.
     */
    goToImage(position) {
        const thisImg = document.querySelectorAll('[data-lightbox="true"][href="' + this.file + '"]'),
            thisArticle = thisImg[0].closest('article'),
            allImgs = thisArticle.querySelectorAll('[data-lightbox="true"]');

        let newIndex = '';

        allImgs.forEach((eachImg, i) => {
            if (eachImg === thisImg[0]) {
                if (typeof (allImgs[i + position]) !== "undefined") {
                    newIndex = i + position;
                } else {
                    return false;
                }
            }
        });

        if (newIndex.length === 0) {
            return false;
        }

        this.el.classList.add('is--loading');
        this.image.remove();
        this.file = allImgs[newIndex].getAttribute('href');
        this.image = this.createImageElement();
        window.setTimeout(() => this.initLoadingIndicator(), 100);
    }

    /**
     * Removes the lightbox
     */
    destruct() {
        window.removeEventListener('keyup', this.bindKeyHandler);
        document.body.classList.remove('is--no-scroll');
        this.el.classList.remove('is--shown');

        let transition = window.getComputedStyle(this.el).getPropertyValue('transition-duration');
        if (transition.match(/(ms)$/g)) {
            transition = parseFloat(transition);
        } else {
            transition = parseFloat(transition)*1000;
        }

        window.setTimeout(() => {
            //this.el.remove();
            this.el.parentNode.removeChild(this.el);
        }, transition);
    }
}
