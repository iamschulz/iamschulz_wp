import {theEnd} from "./_scrolling";
import {enableBoxEvents} from "./_boxes";
import {focus} from "./_boxes";

/**
 * Loads next post from load order array, if available. Else, calls theEnd().
 *
 * @returns {boolean}
 */
export const loadNextPost = () => {
    if(window.offset < window.lO.length) {
        if (sessionStorage.getItem(window.lO[window.offset])) {
            addPost(sessionStorage.getItem(window.lO[window.offset]));
        } else {
            getPost(window.lO[window.offset], window.collapse);
        }
        window.offset += 1;
    } else {
        theEnd();
        return false;
    }
};

/**
 * Gets post information from server
 *
 * @param {int} id - the posts ID
 * @param {boolean} collapse - get excerpt (1) or full post (0)
 */
const getPost = (id, collapse) => {
    let xhr = new XMLHttpRequest(),
        formData = `action=ajax_load_post&post_id=${id}&collapse=${collapse}`;

    const moreBtn = document.getElementById('ctrl-btn');
    moreBtn.classList.add('is--loading');

    xhr.open('POST', window.ajaxurl, true);
    xhr.withCredentials = true;
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(formData);

    xhr.addEventListener("load", () => {
        sessionStorage.setItem(id, xhr.response);
        addPost(xhr.response);
        moreBtn.classList.remove('is--loading');
    });
};

/**
 * Gets post information from server and invokes .box class
 *
 * @param {string} postContent
 */
const addPost = (postContent) => {
    let articleList = document.body.querySelectorAll('article'),
        prevElement = document.body.querySelectorAll('#main-navigation')[0];

    if (articleList.length > 0) {
        prevElement = articleList[articleList.length-1];
    }

    let template = document.createElement('template');
    template.innerHTML = postContent;

    let box = null;

    if (template.content) {
        box = template.content.childNodes[0] //for real browsers
    } else {
        box = template.childNodes[0]; //for IE
    }

    prevElement.parentNode.insertBefore(box, prevElement.nextSibling);
    enableBoxEvents(box);
    focus(window.focusLevel);
};
