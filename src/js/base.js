import {loadCSS} from "./_loadCss";
import {theEnd, loadNext} from "./_scrolling";
import {enableBoxEvents, focus} from "./_boxes";

document.addEventListener('DOMContentLoaded', () => {
    loadCSS(window.themeLink+'/build/style.css');

    document.maxlevel = 10;
    window.theend = false;

    const initialBoxes = document.querySelectorAll('.box');
    for (let i = 0; i < initialBoxes.length; i++) {
        enableBoxEvents(initialBoxes[i]);
    }

    // focus on first box
    focus(document.querySelector('.box').dataset.level);

    if (typeof window.lO === 'undefined' || window.lO.length <= 1) { // for single and pages
        theEnd();
    } else {
        loadNext(0);
    }
});
