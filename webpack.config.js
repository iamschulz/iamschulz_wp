// webpack v4
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const styleCss = new ExtractTextPlugin({filename: 'style.css'});
const critCss = new ExtractTextPlugin({filename: 'critical.css'});

module.exports = {
    entry: {main: './src/js/base.js'},
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'main.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /additional\/.*\.sass$/,
                use: styleCss.extract(
                    {
                        fallback: 'style-loader',
                        use: ['css-loader?url=false', 'sass-loader']
                    })
            },
            {
                test: /critical\/.*\.sass$/,
                use: critCss.extract(
                    {
                        fallback: 'style-loader',
                        use: ['css-loader?url=false', 'sass-loader']
                    })
            }
        ]
    },
    plugins: [
        styleCss,
        critCss
    ]
};
