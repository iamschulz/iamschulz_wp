<?php
/* @package WordPress
 * @subpackage iamschulz2
 * @since iamschulz2 1.0
 */

get_header(); ?>

    <script>
        window.lO = [];
        window.collapse = false;
        window.offset = 0;
    </script>

    <div class="main-content">
        <?php if (have_posts()) : while (have_posts()) : the_post();
            $box = new box(['post_id' => get_the_ID(), 'collapse' => false]);
        endwhile; endif; ?>
    </div>

<?php get_footer(); ?>
