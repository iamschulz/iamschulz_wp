<?php
    $templateDir = get_bloginfo("template_directory");
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php wp_title('&#9774;', true, 'right'); bloginfo('name'); ?></title>
    <meta name="theme-color" content="#dddddd">
    <meta name="msapplication-navbutton-color" content="#dddddd">
    <meta name="apple-mobile-web-app-status-bar-style" content="#dddddd">
    <meta name="description" content="<?php if ( is_single() ) {
            single_post_title('', true);
        } else {
            bloginfo('name'); echo " - "; bloginfo('description');
        }
    ?>" />
    <meta property="og:title" content="<?php wp_title('&#9774;', true, 'right'); bloginfo('name'); ?>" />
    <meta property="og:type" content="article" />
    <?php social_meta_tags(); ?>
    <link rel="alternate" hreflang="de" href="https://iamschulz.de" />
    <link rel="preload" href="<?php echo $templateDir ?>/build/critical.css" as="style" />
    <link rel="preload" href="<?php echo $templateDir ?>/assets/fontawesome_customized.woff2" as="font" type="font/woff2" crossorigin />
    <link rel="preload" href="<?php echo $templateDir ?>/assets/open-sans-v13-latin_cyrillic-regular.woff2" as="font" type="font/woff2" crossorigin />
    <link rel="preload" href="<?php echo $templateDir ?>/assets/open-sans-v13-latin_cyrillic-700.woff2" as="font" type="font/woff2" crossorigin />
    <link rel="stylesheet" href="<?php echo $templateDir ?>/build/critical.css" />
    <link rel="shortcut icon" href="<?php echo $templateDir ?>/assets/fav.ico" />
    <link rel="apple-touch-icon" href="<?php echo $templateDir ?>/assets/fav.ico" />
</head>
<body <?php body_class(); ?>>
    <script>
        window.ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
        window.themeLink = "<?php echo $templateDir ?>";
        window.focusLevel = 1;
    </script>

    <header class="header" id="main-navigation">
        <a class="header__title" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo('name'); ?></a>
        <nav class="header__navigation">
            <ul>
                <?php
                $menuParameters = array(
                    'container'       => false,
                    'echo'            => true,
                    'items_wrap'      => '%3$s'
                );
                wp_nav_menu($menuParameters);
                ?>
            </ul>
        </nav>
    </header>
    <main id="main">
