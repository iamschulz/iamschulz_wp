# iamschulz v2

## wat?
This is a custom wordpress theme. it's in production at https://iamschulz.de

## Features
- Endless Scrolling through posts
- Posts are sorted by categories
- nothing much else
- it's fast though

## How to use
- images in posts are substituted with srcsets 
    - you need to add webps to your posts jpgs
    - add the classes `focus-bottom` or `focus-top` to the imgs in your posts to crop thumbnails correctly
    - linked images will open in a modal
    - supported image formats are as follows: 'jpg' , 'jpeg', 'png', 'bmp', 'gif', 'svg', 'webp'
- youtube links will also be subsituted with a one-click replacer

## How to build
- `npm install`
- `npm run dev`
    - process js and css, add sourcemaps, start a watcher
- or `npm run build`
    - process js and css, minify
    
## To do
- implement PWA to support offline first
- get data from WP Rest API and store out side from wp in faster individual app

## Questions?
- hallo@iamschulz.de
