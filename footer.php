        <a id="ctrl-btn" class="is--up" href="#main">Nach oben</a>
    </main>
    <?php
        wp_footer();
        $templateDir = get_bloginfo("template_directory");
    ?>
    <script defer src="<?php echo $templateDir ?>/build/main.js"></script>
    <noscript>
        <style> @import url("<?php echo $templateDir ?>/build/style.css"); </style>
    </noscript>
</body>
</html>
